# podstawy-programowania-python

## Przygotowanie srodowiska pracy

### Visual Studio Code

- Pobierz i zainstaluj program [Visual Studio Code (Microsoft)](https://code.visualstudio.com)

- Otwórz program `Visual Studio Code` i przejdz do zakladki rozszerzen

![ext1](imgs/ext1.png)

- Zainstaluj rozszerzenia `Python` (Microsoft), `Dev Containers` (Microsoft) i `Jupyter` (Microsoft)

![vsc1](imgs/vsc1.png)

![vsc2](imgs/vsc2.png)

### Docker Desktop

- Pobierz i zainstaluj program [Docker Desktop](https://www.docker.com/products/docker-desktop/)

- Uzyj ustawien zalecanych

![dd1](imgs/dd1.png)

- Kontunuuj bez logowania (rejestracji)

![dd2](imgs/dd2.png)

- Wprowadz dane dot. sposobu uzytkowania aplikacji

![dd3](imgs/dd3.png)

- Uruchom program `Docker Desktop` i pozostaw go uruchomionego (mozna zminimalizowac)


## Pobierz materialy

- materialy znajduja sie w [repozytorium](https://gitlab.com/jankosinski/podstawy-programowania-python) udostepnionym w serwisie `gitlab`

- pobierz materialy jako skompresowane archiwum `.zip`

![gitlab1](imgs/gitlab1.png)

![gitlab2](imgs/gitlab2.png)

- rozpakuj archiwum (na wiekszosci systemow operacyjnych wystarczy kliknac dwukrotnie lewym przyciskiem myszy)

- otworz terminal w rozpakowanym katalogu/folderze. Na systemie windows otworz folder z plikami, przejdz do paska adresu (`ctr+l`) i wpisz `cmd`. Na systemie Mac OS otworz terminal i uzyj polecenia `cd /sciezka/do/katalogu/z/danymi`

- Stwórz i uruchom kontener dockera (uruchamiając ponizsze polecenia w terminalu)


```{bash}
docker build -t podstawy-programowania-python .
```

```{bash}
docker compose up -d
```

## Uruchomienie srodowiska pracy

- Uruchom program `Docker Desktop`

- Upewnij sie ze kontener `podstawy-programowania-python` jest uruchomiony (zielony kolor, status `Running`)

![dd5](imgs/dd5.png)

- Jezeli nie jest uruchomiony, kliknij przycisk `Uruchom` (play)

![dd4](imgs/dd4.png)

- Uruchom program `Visual Studio Code` i otworz `paletę poleceń` (`View` -> `Command Palette`; `Wyswietl` -> `Paleta polcen`)

- Poszukaj komendy `Dev Containers: Attach to Running Container ...` (`Kontenery deweloperskie: Dolacz do Uruchomionego Kontenera ...`) i potwierdz wykonanie przyciskiem `Enter`

![pp1](imgs/pp1.png)

- Wybierz z listy kontener o nazwie `/podstawy-programowania-python` i potwierdz wykonanie przyciskiem `Enter` (powinno otworzyc sie nowe okno programu `Visual Studio Code`)

![pp2](imgs/pp2.png)

- Zamknij dotychczasowe okno `Visual Studio Code` i przejdz do nowo-otwartego

- Poczekaj az kontener sie zaladuje (w lewym dolnym rogu w trakcie ladowania bedzie komunikat na niebieskim (lub zielonym) tle `Opening Remote ...` (`Trwa otwieranie lokalizacji zdalnej ...`))

- Po zaladowaniu komunikat na niebieskim (lub zielonym) tle zmieni sie na `Container podstawy-programowania-python` (`Kontener podstawy-programowania-python`)

- Otworz zakladke eksploratora plikow w `Visual Studio Code`

![vsc3](imgs/vsc3.png)

- Otworz katalog (`/uo/python/`), w ktorym znajduja sie pliki z materialami (`Open folder` lub `Otwórz folder`)

![vsc4](imgs/vsc4.png)

- W efekcie powinnismy zobaczyc liste plikow potrzebnych do dalszej pracy

![vsc5](imgs/vsc5.png)

- Upewnij się, ze rozszerzenia `Jupyter` i `Python` zostaly pomyslnie zainstalowane w kontenerze. Jezeli nie, zainstaluj je ponownie (tym razem dostepna bedzie opcja `Install inside container` albo `Zainstaluj w kontenerze`)

![j1](imgs/j1.png)

- Wroc do zakladki ekploratora plikow w `Visual Studio Code` i wybierz plik `test.ipynb`

- Kliknij na komorke zawierająca kod `print(4+4)` i wywolaj ja

![j2](imgs/j2.png)

- Jezeli na ekranie pojawi sie monit `Select kernel` lub `Wybierz jadro`, wybieramy opcje `Existing Python Environments` lub `Srodowiska jezyka Python` i `/usr/local/bin/python`. Instalujemy rekomendowane rozszerzenia i ponownie probujemy wywolac ww. komorke. Wynikiem powinna byc liczba 8 wyswietlona ponizej

![j3](imgs/j3.png)

- Gratulacje! Jestesmy gotowi na zajecia!




