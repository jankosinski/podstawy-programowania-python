import streamlit as st
import pandas as pd
from geopy.geocoders import Nominatim
import pycountry
from countryinfo import CountryInfo

st.title('Mappy is :blue[cool] :sunglasses:')

geolocator = Nominatim(user_agent="Mappy")

location = st.text_input('Location')

if st.button("Locate") and location != "":
    try:
        location = geolocator.geocode(location, language="en")
        #st.write(location.address)
        #st.write(location.latitude)
        #st.write(location.longitude)
        country_name = location.address.split(", ")[-1]
        country = pycountry.countries.get(name=country_name)
        st.write('The current location is', location)
        st.write(f"{country.flag} {country.flag} {country.flag} {country.flag} {country.flag} {country.flag}")
        st.write(f"You are in: {country.official_name}")
        country_object = CountryInfo(country_name)
        st.info(f"Capital of {country_name} is {country_object.capital()}")
        st.info(f"Population of {country_name} is {country_object.population()}")
        st.info(f"Languages of {country_name} are {country_object.languages()}")

        df = pd.DataFrame(
            [(location.latitude, location.longitude)],
            columns=['lat', 'lon']
        )

        st.map(df)
    except AttributeError:
        st.error("Couldn't find location")

    