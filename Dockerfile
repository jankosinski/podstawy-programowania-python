FROM python

RUN mkdir -p /uo/python
COPY . /uo/python

LABEL NAME="podstawy-programowania-python"
LABEL MAINTAINER="jankos@amu.edu.pl"
LABEL VERSION="1.0.0"

RUN apt-get update
ENV HOME /uo/python

WORKDIR /uo/python

ENTRYPOINT ["/bin/bash"]