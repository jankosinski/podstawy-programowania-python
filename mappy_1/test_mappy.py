from mappy import get_country_name_from_address

def test_get_country_name_from_address():
    assert get_country_name_from_address("London, Greater London, England, United Kingdom") == "United Kingdom", "Should be United Kingdom"

test_get_country_name_from_address()