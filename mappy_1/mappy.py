import streamlit as st
import pandas as pd
import numpy as np
from geopy.geocoders import Nominatim
from countryinfo import CountryInfo







def get_latitude(location):
    location = geolocator.geocode(location, language="en")
    return location.latitude

def get_longitude(location):
    location = geolocator.geocode(location, language="en")
    return location.longitude

def get_address(location):
    location = geolocator.geocode(location, language="en")
    return location.address

def get_country_name_from_address(address):
    country = address.split(",")[-1].strip()
    return country




geolocator = Nominatim(user_agent="mappy")

st.title('Mappy is :blue[cool] :sunglasses:')

location = st.text_input('Location')
st.write('The current location is', location)



if st.button("Locate"):
    #st.info(location)
    address = get_address(location)
    country = get_country_name_from_address(address)

    st.info(f"Your address is: {address}")
    
    st.info(f"Displaying information about: {country}")

    country_object = CountryInfo(country)
    st.info(f"Capital of {country} is {country_object.capital()}")
    st.info(f"Population of {country} is {country_object.population()}")
    st.info(f"Languages of {country} are {country_object.languages()}")
    #st.info(location.latitude)
    #st.info(location.longitude)

    df = pd.DataFrame(
        [(get_latitude(location), get_longitude(location))],
        columns=['lat', 'lon'])

    st.map(df)